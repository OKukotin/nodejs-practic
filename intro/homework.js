const N = 5;
const MAX = 101;

function createMatrix(rows, cols) {
    let numbers = [];
    for (let i = 0; i < rows; i++) {
        numbers[i] = [];
        for (let j = 0; j < cols; j++) {
            numbers[i][j] = Math.floor(Math.random() * Math.floor(MAX));
        }
    }
    return numbers;
}

function calculate(numbers) {
    let upperMainDiagonal = 0;
    let underSideDiagonal = 0;
    for (let i = 0; i < N; i++) {
        for (let j = 0; j < N; j++) {
            if (i < j) {
                upperMainDiagonal += numbers[i][j];
            }
            if (i + j >= N) {
                underSideDiagonal += numbers[i][j];
            }
        }
    }
    const result = Math.abs(upperMainDiagonal - underSideDiagonal);
    console.log("Sum of elements upper main diagonal: ", upperMainDiagonal);
    console.log("Sum of elements under side diagonal: ", underSideDiagonal);
    console.log("The absolute value of the difference of previous numbers: ", result);
}
const matrix = createMatrix(N, N);
calculate(matrix);