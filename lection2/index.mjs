import * as test from 'test-utils';
import utils from 'util';

const params = {
    password: 'df345w43jkj3443d'
};

test.runMePlease(params, response => {
    console.log(response);
});

const promiseFunction = utils.promisify(test.runMePlease);
try {
    const result = await promiseFunction(params);
} catch (error) {
    console.log('error', error);
}